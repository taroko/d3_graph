/**
 *  @file Jhhd3_graph/js/d3js
 *  @brief 管理 d3 svg 畫圖，設定 config, 管理物件
 *  @author C-Salt Corp.
 */

var color1 = 
[
	"#0000ff","#996633","#00ffff","#00ff00","#ff00ff","#ff7f00","#7f007f","#ffff00"
	,"#cccccc","#999999","#666666","#333333","#ffcccc","#cc9999","#996666","#663333","#330000","#ff9999"
	,"#cc6666","#cc3333","#993333","#660000","#ff6666","#ff3333","#ff0000","#cc0000","#990000","#ff9966"
	,"#ff6633","#ff3300","#cc3300","#993300","#ffcc99","#cc9966","#cc6633","#996633","#663300","#ff9933"
	,"#ff6600","#ff9900","#cc6600","#cc9933","#cccc99","#999966","#666633","#333300","#ffff99","#cccc66"
	,"#cccc33","#999933","#666600","#ffff66","#ffff33","#ffff00","#cccc00","#999900","#ccff66","#99cc00"
	,"#669900","#99cc33","#336600","#33cc00","#99cc99","#ccffcc","#003300","#33cc66","#33ff99","#99ffcc"
	,"#ccffff","#99cccc","#669999","#336666","#003333","#99ffff","#66cccc","#33cccc","#339999","#66ccff"
	,"#33ccff","#0099cc","#006699","#99ccff","#6699cc","#3399cc","#336699","#003366","#3399ff","#0099ff"
	,"#0066ff","#0066cc","#6699ff","#3366cc","#0066cc","#0033cc","#003399","#ccccff","#9999cc","#666699"
	,"#333366","#6666cc","#000066","#333399","#6666ff","#3333ff","#9966ff","#9966cc","#6633cc","#663399"
	,"#9933ff","#cc66ff","#9933cc","#cc33ff","#9900cc","#ffccff","#cc99cc","#996699","#663366","#330033"
	,"#ff99ff","#cc66cc","#cc33cc","#993399","#ff66ff","#ff66cc","#ff33cc","#ff00cc","#cc6699","#cc3399"
	,"#cc0099","#cc6699","#cc3399","#cc3366","#ff3366","#cc0033","#990033"
];
var color2 = 
[
	"#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#ff9896", "#9467bd", "#17becf", "#9edae5",
	"#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d",
	"#DC00D9", "#DC0300", "#DC7100", "#D9DC00", "#00DC03", "#00D9DC", "#006BDC", "#0300DC", "#7100DC",
	"#FF37FD", "#FF3937", "#FF9D37", "#FDFF37", "#37FF39", "#37FDFF", "#3799FF", "#3937FF", "#9D37FF",
	"#FF91FE", "#FF9291", "#FFC991", "#FEFF91", "#91FF92", "#91FEFF", "#91C7FF", "#9291FF", "#C991FF",
	"#FFEBFF", "#FFEBEB", "#FFF5EB", "#FFFFEB", "#EBFFEB", "#EBFFFF", "#EBF5FF", "#EBEBFF", "#F5EBFF"
];

var color3 = 
[
	"#FF19FC", "#FF1C19", "#FCFF19", "#19FF1C", "#19FCFF", "#1C19FF", 
	"#B266B1", "#B26766", "#B1B266", "#66B267", "#66B1B2", "#6766B2",
	"#FF91FE", "#FF9291", "#FEFF91", "#91FF92", "#91FEFF", "#9291FF", 
	"#DAB6DA", "#DAB6B6", "#DADAB6", "#B6DAB6", "#B6DADA", "#B6B6DA", 
	"#DC006B", "#DC7100", "#6BDC00", "#00DC71", "#006BDC", "#7100DC", 
	"#C68EA9", "#C6AB8E", "#A9C68E", "#8EC6AB", "#8EA9C6", "#AB8EC6", 
	"#924A6D", "#926F4A", "#6D924A", "#4A926F", "#4A6D92", "#6F4A92",
	"#FF55A8", "#FFAC55", "#A8FF55", "#55FFAC", "#55A8FF", "#AC55FF"
];

define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"d3_graph/js/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
		,"d3_graph/js/d3/d3_strategy"
		,"d3_graph/js/d3/d3_controlpanel"
		,"d3_graph/js/d3/barchart"
		,"d3_graph/js/d3/grouped_barchart"
		,"d3_graph/js/d3/normalized_barchart"
		,"d3_graph/js/d3/stacked_barchart"
		,"d3_graph/js/d3/piechart"
		,"d3_graph/js/d3/scatterchart"
		,"d3_graph/js/d3/scatter_matrix"
		
	],function(declare, lang, d3, D3_helper, D3_Strategy, D3_ControlPanel){
		return declare( "D3", [D3_helper, D3_Strategy],
			{
				/**
				 * @brief 創建時執行，給予預設參數，並融合處理自定參數
				 * @param config 傳入的設定物件
				 * @return nul
				 */
				 /// !constructor
				constructor : function(config)
				{
					this.set_window_size();
					if(config)
					{
						this.merge_config(this.config, config);
					}
				}
				,set_window_size: function()
				{
					var config = {};
					config.width = 1200;
					config.height = 600;
					config.margin = {top: 60, right: 240, bottom: 100, left: 170};
					this.config = config;
				}
				,data: []
				,draw: function(draw_type, draw_obj, data, nodata_warning)
				{
					// 存資料 data
					this.origin_data = new Array();
					this.data = new Array();
					this.nodata_warning = "Please set the plot area from sheet";
					if(nodata_warning) this.nodata_warning = nodata_warning;
					
					// 點擊，隱藏的 column raw
					this.hide_data = new Object({"column":{}, "raw":{} });
					
					this.is_transposed = false;
					
					if(draw_type)
						this.config.draw_type = draw_type;
					
					if(draw_obj)
						this.config.draw_obj = draw_obj;
					
					if(data)
						this.data = data;
					
					if(! this.check_basic_parameter() )
					{
						console.log( "Error, some parameter error \n" );
						return false;
					}
					
					/// @brief 計算實際物件大小，也就是扣掉 margin 的大小
					this.cal_margin_size();
					
					/// @brief 創建 config.window and config.control
					this.create_basic_window();
										
					console.log("Draw D3 Chart", draw_obj, draw_type);
					
					//this.graph = eval("new " + this.config.draw_type + "(this.config)")
					//this.graph.draw(data);
					
					var strategy = this.strategy = this.draw_type_strategies(draw_type);					
					this.config.control_panel = new D3_ControlPanel(this);
					
					// if no data, draw No data tips
					if(this.draw_nodata()) return;
					
					this.make(strategy);
				}
				//35, -3
				,draw_type_strategies: function(draw_type)
				{
					var strategy = {};
					
					if(draw_type == "BarChart")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": {"domain": "1st_column", "range": "rangeRoundBands"}
								,"y0": {"domain": "max_2st_column", "range": "rangeRound"}
							}
							,"draw_axis": {
								"type": "xy"
								,"x": ""
								,"y": ""
							}
							,"draw_content": {
								"type": draw_type
							}
						}
					}
					else if(draw_type == "StackedBarChart")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": {"domain": "1st_column", "range": "rangeRoundBands"}
								,"y0": {"domain": "max_data_array_total", "range": "rangeRound"}
							}
							,"data_handl": {
								"type": draw_type
								,"limit_column": 0
								//,"transpose": true
							}
							,"draw_axis": {
								"type": "xy"
								,"x": {"limit":0}
								,"y": {"limit":0}
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "default"
								,"limit": 25
							}
							,"redraw": {
								"redraw_content": {"type": draw_type}
								,"redraw_axis": {
									"type": "xy"
									,"y": ""
								}
							}
						}
					}
					else if(draw_type == "GroupedBarChart")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": {"domain": "1st_column", "range": "rangeRoundBands"}
								,"x1": {"domain": "1st_raw", "range": "rangeRoundBands"}
								,"y0": {"domain": "max_all", "range": "rangeRound"}
							}
							,"data_handl": {
								"type": draw_type
							}
							,"draw_axis": {
								"type": "xy"
								,"x": {"limit":0}
								,"y": {"limit":0}
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "default"
							}
							
						}
					}
					else if(draw_type == "NormalizedBarChart")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": {"domain": "1st_column", "range": "rangeRoundBands"}
								,"y0": {"domain": "normalized", "range": "rangeRound"}
							}
							,"data_handl": {
								"type": draw_type
							}
							, "draw_axis": {
								"type": "xy"
								,"x": {"limit":0}
								,"y": {"limit":0}
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "default"
							}
						}
					}
					else if(draw_type == "ScatterChart")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": "min_max_2st_column"
								,"y0": "min_max_3st_column"
							}
							,"draw_axis": {
								"type": "xy"
								,"x": ""
								,"y": ""
							}
						}
					}
					else if(draw_type == "PieChart_multi")
					{
						strategy = {
							"make_axis": {
								"type": "pie"
								,"pie": "multi_layer"
								,"arc": "multi_layer"
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "column"
							}
						}
					}
					else if(draw_type == "PieChart")
					{
						strategy = {
							"make_axis": {
								"type": "pie"
								,"pie": "single_layer"
								,"arc": "single_layer"
							}
							,"data_handl": {
								"type": draw_type
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "pie_column_center"
							}
						}
					}
					else if(draw_type == "PieChart_column")
					{
						strategy = {
							"make_axis": {
								"type": "pie"
								,"pie": "single_layer"
								,"arc": "single_layer"
							}
							,"draw_content": {
								"type": draw_type
							}
							,"draw_legend": {
								"type": "pie_column_center"
							}
						}
					}
					else if(draw_type == "SequenceLogo")
					{
						strategy = {
							"make_axis": {
								"type": "xy"
								,"x0": {"domain": "1st_column", "range": "rangeRoundBands"}
								,"y0": {"domain": "max_data_array_total", "range": "rangeRound"}
							}
							,"data_handl": {
								"type": "StackedBarChart"
							}
							,"draw_axis": {
								"type": "xy"
								,"x": {"limit":0}
								,"y": {"limit":0}
							}
							,"draw_content": {
								"type": draw_type
							}
							//,"draw_legend": {
							//	"type": "default"
							//	,"limit": 25
							//}
						}
					}
					if(strategy == {})
					{
						console.log("Error, draw_type not found");
						return;
					}
					return strategy;
				}
				,check_basic_parameter: function()
				{
					if(!this.config.draw_type)
					{
						console.log("Error, please set draw_type");
						return false;
					}
					if(!this.config.draw_obj)
					{
						console.log("Error, please set draw_obj");
						return false;
					}
					if(!this.config.filename && !this.data)
					{
						console.log("Error, please set data file");
						return false;
					}
					if(! this.check_draw_obj() )
					{
						return false;
					}
					return true;
				}
				/**
				 * @brief 確定 draw_obj，如果是字串，利用 d3.select 選取物件
				 * @param null
				 * @return null
				 */
				/// !check_draw_obj
				,check_draw_obj : function()
				{
					if(typeof(this.config.draw_obj) == "string")
					{
						var obj = d3.select(this.config.draw_obj);
						if(obj[0].length == 0 || obj[0][0] == null)
						{
							console.log("Error!, no such object");
							return false;
						}
						this.config.draw_obj = obj;
					}
					return true;
				}
				
				/**
				 * @brief 計算真實的長寬與圓餅圖中心、半徑
				 * @param null
				 * @return null
				 */
				/// !cal_margin_size
				,cal_margin_size : function()
				{
					var config = this.config;
					config.width_ = config.width - config.margin.left - config.margin.right;
					config.height_ = config.height - config.margin.top - config.margin.bottom;
					config.radius = Math.min(config.width_ + config.margin.left*2/3, config.height_ +config.margin.top*2/3) / 2 ;
					config.center = config.width_/2 + config.margin.left;
				}
				
				/**
				 * @brief 設定 svg viewbox 與創建下層 g window (圖層, 物件畫在下一層，方便管理), g control (控制層)  
				 * @param null
				 * @return null
				 */
				/// !create_window_control
				,create_basic_window : function()
				{
					var thisA = this;
					
					/// @brief this.config.window = <g></g>, 多一層 g, 物件畫在此
					this.config.window = this.config.draw_obj
					.attr("viewBox", "0 0 " + this.config.width + " " + this.config.height)
					.append("g")
					.attr("transform", "translate(" + this.config.margin.left + "," + this.config.margin.top + ")");
				}
				
				/**
				 * @brief 設定 svg 一些預設參數
				 * @param null
				 * @return null
				 */
				 /// !set_default_config
				//,set_default_config : function()
				//{
				//	var config = {};
				//	
				//	config.width = 1000;
				//	config.height = 600;
				//	config.margin = {top: 40, right: 180, bottom: 80, left: 80};
				//	config.color = d3.scale.ordinal().range(color2);
				//	//config.color = d3.scale.category20();
				//	
				//	config.x0 = d3.scale.ordinal();
				//	config.x1 = d3.scale.ordinal();
				//	config.y0 = d3.scale.linear();
				//	config.xAxis = d3.svg.axis().scale(config.x0).orient("bottom");
				//	config.yAxis = d3.svg.axis().scale(config.y0).orient("left");
				//	
				//	/// @brief svg 中的 "點" 的直徑
				//	config.r = 3;
				//	config.arc = d3.svg.arc();
				//	config.pie = d3.layout.pie()
				//	
				//	this.config = config;
				//}
				
				/**
				 * @brief 畫圖，會創建一個子物件，是真正畫 svg 的物件
				 * @param draw_type （可以在 contruct 時設定）子物件名稱，也就是要畫的圖的種類, e.g., BarChart, GroupedBarChart, PieChart, ScatterChart ,...
				 * @param draw_obj （可以在 contruct 時設定）要畫上去的 dom 物件 id, e.g., #svg. 或是 d3.select 後的物件
				 * @param data 直接給予要畫圖的 data，不設定的話，必須設定 config.filename, 畫圖時會自動讀檔
				 * @return null
				 */
				 /// !draw
				,draw2 : function(draw_type, draw_obj, data)
				{
					
					if(draw_type)
					{
						this.config.draw_type = draw_type;
					}
					if(draw_obj)
					{
						this.config.draw_obj = draw_obj;
					}
					if(data)
					{
						this.data = data;
					}	
					
					if(!this.config.draw_type)
					{
						console.log("Error, please set draw_type");
						return;
					}
					if(!this.config.draw_obj)
					{
						console.log("Error, please set draw_obj");
						return;
					}
					if(!this.config.filename && !this.data)
					{
						console.log("Error, please set data file");
						return;
					}
					if(! this.check_draw_obj() )
					{
						return ;
					}
					/// @brief 計算實際物件大小，也就是扣掉 margin 的大小
					this.cal_margin_size();
					
					/// @brief 創建 config.window and config.control
					this.create_basic_window();
										
					console.log("Draw D3 Chart", draw_obj, draw_type, data);
					this.graph = eval("new " + this.config.draw_type + "(this.config)")
					this.graph.draw(data);
					
				}
				
				
				
				/**
				 * @brief 現在物件版本
				 * @param null
				 * @return null
				 */
				/// !version
				,version : function()
				{
					var version = "2.0";
					return version;
				}
				
			}
			
		);
	
	}
);
