//d3_graph/js/d3/barchar.js

define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
	],function(declare, lang, d3, D3_helper){
		return declare( "NormalizedBarChart", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
					//console.log("D3 NormalizedBarChart constructor");
				}
				,draw : function(data)
				{
				
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
	
				}
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data)
					{
						var thisA = this;
						this.data = data;
						
						var data_1st_key_name = this.get_data_key_name(data, 0);
						var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });

						thisA.config.color
						.domain(data_key_names);
						
						this.adjust_data(data_key_names);
						
						data.forEach(function(d) {
						    var y0 = 0;
						    d.data_array = thisA.config.color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
						    d.data_array.forEach(function(d) { d.y0 /= y0; d.y1 /= y0; });
						});
						
						//data.sort(function(a, b) { return b.data_array[0].y1 - a.data_array[0].y1; });
						
						this._setup_xy();
						
						thisA.config.window.append("g")
						.attr("class", "x_axis")
						.attr("transform", "translate(0," + thisA.config.height_ + ")")
						.call(thisA.config.xAxis);
						
						thisA.config.window.append("g")
						.attr("class", "y_axis")
						.call(thisA.config.yAxis)
						
						
						var state = thisA.config.window
						.append("g").attr("class", "main")
						.selectAll(".state")
					      .data(data)
					    .enter().append("g")
					      .attr("class", function(d){return "state mouse_event_row item_class item_class_"+thisA.encode(d[data_1st_key_name]);})
					      .attr("transform", function(d) { return "translate(" + thisA.config.x0(d[data_1st_key_name]) + ",0)"; });
					      
					    state.selectAll("rect")
					      .data(function(d) { return d.data_array; })
					    .enter().append("rect")
					      .attr("class", function(d){return "item_class mouse_event item_class_"+thisA.encode(d.name);})
					      .attr("width", thisA.config.x0.rangeBand())
					      .attr("y", function(d) { return thisA.config.y0(d.y1); })
					      .attr("height", function(d) { return thisA.config.y0(d.y0) - thisA.config.y0(d.y1); })
					      .style("fill", function(d) { return thisA.config.color(d.name); })
					      .attr("title", function(d) { return (d.y1 - d.y0).toFixed(3); });
					      
						thisA.append_legend();
						
						thisA.mouse_event($(thisA.config.window[0]));
						thisA.add_png_button();
						thisA.x_axis_rotate();
					});
				}
				,append_legend : function()
				{
					var thisA = this;
					var legend = thisA.config.window.select(".state:last-child")
					.selectAll(".legend")
					.data(function(d) { return d.data_array; })
					.enter().append("g")
					.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d.name);})
					.attr("transform", function(d) { return "translate(" + thisA.config.x0.rangeBand() / 2 + "," + thisA.config.y0((d.y0 + d.y1) / 2) + ")"; })
					.style("fill",function(d) { return thisA.config.color(d.name); });
					
					legend.append("line")
					.style("stroke", "#000")
					.style("shape-rendering","crispEdges")
					.attr("x2", thisA.config.x0.rangeBand()/2+10);
					
					legend.append("text")
					.attr("x", thisA.config.x0.rangeBand()/2+13)
					.attr("dy", ".35em")
					.text(function(d) { return d.name; });
				}
				,_setup_xy : function()
				{
					var thisA = this;
					
					var data_1st_key_name = this.get_data_key_name(this.data, 0);
					var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
					
					thisA.config.x0
					.domain(this.data.map(function(d) { return d[data_1st_key_name]; }))
					.rangeRoundBands([0, thisA.config.width_], 0.1 );

					thisA.config.y0
					.rangeRound([thisA.config.height_, 0]);

					thisA.config.yAxis
					.tickFormat(d3.format(".0%"));
				}
			}
		);
	
	}
);
