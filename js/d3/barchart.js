//Jhhd3_graph/js/d3/barchar.js

define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
	],function(declare, lang, d3, D3_helper){
		return declare( "BarChart", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
					//console.log("D3 BarChar constructor");
				}
				/**
				 * @brief 讀檔畫圖，如果 data 沒有設定，則讀檔
				 * @param data d3 data object
				 * @return null
				 */
				/// !draw
				,draw : function(data)
				{
				
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
	
				}
				
				/**
				 * @brief 畫圖實作，x軸 y軸 內容 圖標
				 * @param error Error callback, not impl
				 * @param data d3 data object
				 * @return function 
				 */
				/// !draw_impl
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data)
					{
						var thisA = this;
						this.data = data;
						
						var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
						var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
						
						console.log(data_1st_key_name, data_2st_key_name);
						/// @brief 主要目的為 string to int
						this.adjust_data([data_2st_key_name]);
						
						// @brief 設定 x0, y0, x axis, y axis
						this._setup_xy();
						
						/// @brief draw x axis
						thisA.config.window.append("g")
						.attr("class", "x_axis")
						.attr("transform", "translate(0," + this.config.height_ + ")")
						.call(this.config.xAxis);
						
						/// @brief draw y axis
						thisA.config.window.append("g")
						.attr("class", "y_axis")
						.call(this.config.yAxis)
						.append("text")
						.attr("transform", "rotate(-90)")
						.attr("y", 6)
						.attr("dy", ".71em")
						.style("text-anchor", "end")
						.text(data_2st_key_name);
						
						/// @draw main content
						thisA.config.window
						.append("g").attr("class", "main")
						.selectAll(".bar")
						.data(data)
						.enter().append("rect")
						.attr("class", function(d){ return "bar mouse_event item_class item_class_" + thisA.encode(d[data_1st_key_name]);})
						.attr("x", function(d) { return thisA.config.x0(d[data_1st_key_name]); })
						.attr("width", thisA.config.x0.rangeBand())
						.attr("y", function(d) { return thisA.config.y0(d[data_2st_key_name]); })
						.attr("height", function(d) { return thisA.config.height_ - thisA.config.y0(d[data_2st_key_name]); })
						.attr("title", function(d) { return d[data_2st_key_name]; })
						.attr("fill", "#1f77b4");
						
						/// @brief define in d3_helper
						thisA.mouse_event($(thisA.config.window[0]));
						
						/// @brief define in d3_helper
						thisA.add_png_button();
						
						/// @brief define in d3_helper
						thisA.x_axis_rotate();
					});
				}
				,append_legend : function()
				{}
				
				/**
				 * @brief 設定座標軸的 domain (value max min), range (螢幕位置寬高)
				 * @param null
				 * @return null
				 */
				/// !_setup_xy
				,_setup_xy : function()
				{
					var thisA = this;
					
					var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
					var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
					
					thisA.config.x0
					.domain(thisA.data.map(function(d) { return d[data_1st_key_name]; }))
					.rangeRoundBands([0, thisA.config.width_], .1);
					
					var max_value = d3.max(thisA.data, function(d) { return d[data_2st_key_name]; });
					
					thisA.config.y0
					.domain([0, max_value])
					.rangeRound([thisA.config.height_, 0]);
					
					/// @brief y軸上 tick 數量
					if(max_value < 1)
						thisA.config.yAxis.ticks(10, "%");
					else
						thisA.config.yAxis.tickFormat(d3.format(".3s"));
				}
			}
			
		);
	
	}
);
