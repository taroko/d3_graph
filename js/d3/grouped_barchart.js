//d3_graph/js/d3/barchar.js

define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
	],function(declare, lang, d3, D3_helper){
		return declare( "GroupedBarChart", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
				}
				,draw : function(data)
				{
				
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
	
				}
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data)
					{
						var thisA = this;
						this.data = data;
						
						var data_1st_key_name = this.get_data_key_name(data, 0);
						var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
						
						this.adjust_data(data_key_names);
						
						//make data_array
						this.data.forEach(function(d) {
						  d.data_array = data_key_names.map(function(name) { return {name: name, value: +d[name]}; });
						});
						
						this._setup_xy();
						
						thisA.config.window.append("g")
						.attr("class", "x_axis")
						.attr("transform", "translate(0," + thisA.config.height_ + ")")
						.call(thisA.config.xAxis);
						
						thisA.config.window.append("g")
						.attr("class", "y_axis")
						.call(thisA.config.yAxis)
						.append("text")
						.attr("transform", "rotate(-90)")
						.attr("y", 6)
						.attr("dy", ".71em")
						.style("text-anchor", "end")
						.text("Frequency");
						
						var state = thisA.config.window
						.append("g").attr("class", "main")
						.selectAll(".state")
					      .data(data)
					    .enter().append("g")
					      .attr("class", function(d){return "g item_class mouse_event_row item_class_"+thisA.encode(d[data_1st_key_name]);})
					      .attr("transform", function(d) { return "translate(" + thisA.config.x0(d[data_1st_key_name]) + ",0)"; });
					      
					    state.selectAll("rect")
					      .data(function(d) { return d.data_array; })
					    .enter().append("rect")
					      .attr("class", function(d){return "item_class mouse_event item_class_"+thisA.encode(d.name);})
					      .attr("width", thisA.config.x1.rangeBand())
					      .attr("x", function(d) { return thisA.config.x1(d.name); })
					      .attr("y", function(d) { return thisA.config.y0(d.value); })
					      .attr("height", function(d) { return thisA.config.height_ - thisA.config.y0(d.value); })
					      .style("fill", function(d) { return thisA.config.color(d.name); })
					      .attr("title", function(d) { return d.value; } );
					      
					    
						thisA.append_legend(data_key_names);
						
						thisA.mouse_event($(thisA.config.window[0]));
						thisA.add_png_button();
						thisA.x_axis_rotate();
						
					});
				}
				,append_legend : function(data_key_names)
				{
					var thisA = this;
					var legend = thisA.config.window.selectAll(".legend")
					.data(data_key_names.slice().reverse())
					.enter().append("g")
					.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
					.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; })
					.style("fill", function(d){return thisA.config.color(d)});
					
					legend.append("rect")
					//.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
					.attr("x", thisA.config.width_ - 18 +20)
					.attr("width", 18)
					.attr("height", 18)
					.style("fill", thisA.config.color);
					
					legend.append("text")
					.attr("x", thisA.config.width_ - 24 + 48)
					.attr("y", 9)
					.attr("dy", ".35em")
					.style("text-anchor", "start")
					.text(function(d) { return d; });
				}
				,_setup_xy : function()
				{
					var thisA = this;
					
					var data_1st_key_name = this.get_data_key_name(this.data, 0);
					var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
					
					thisA.config.x0
					.domain(this.data.map(function(d) { return d[data_1st_key_name]; }))
					.rangeRoundBands([0, thisA.config.width_], 0.1 );
					
					thisA.config.x1
					.domain(data_key_names)
					.rangeRoundBands([0, this.config.x0.rangeBand()]);
					
					var max_value = d3.max(this.data, function(d) { return d3.max(d.data_array, function(d) { return d.value; }); });
					
					thisA.config.y0
					.domain([1, max_value])
					.rangeRound([thisA.config.height_, 0]).nice();
					//.range([thisA.config.height_, 0]); //same
					
					thisA.config.yAxis = d3.svg.axis().scale(thisA.config.y0).orient("left");
					
					if(max_value < 1)
						thisA.config.yAxis.ticks(12, "%");
					else
						thisA.config.yAxis.tickFormat(d3.format(".3s"));
				
				}
			}
		);
	
	}
);
