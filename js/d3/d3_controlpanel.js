/**
 *  @file d3_graph/js/d3/d3_helper.js
 *  @brief 常用功能
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"d3_graph/js/d3.v3.min.js"
	],function(declare, lang, d3){
		return declare( "D3_ControlPanel", null,
			{
				thisP: {}
				,config: 
				{
					
				}
				,constructor : function(parent)
				{
					if(!parent)
					{
						console.log("Error D3 control panel need parent obj");
					}
					this.thisP = parent;
					console.log("ControlPanel constructor", this.thisP);
					
					this.add_control_panel();
					//this.add_png_button();
				}
				,add_control_panel: function()
				{
					var thisA = this;
					var thisP = this.thisP;
					/// @brief this.config.control = <g></g>, 多一層 g, 控制層與圖層並列
					thisA.control_obj = thisP.config.draw_obj
					.append("g")
					.attr("class", "control")
					.attr("transform", "translate(" + (thisP.config.width - 50) + ", " + 60 + ")")
					.attr("style", "cursor:pointer;");
					
					this.add_control_panel_main();
					this.add_control_panel_button();
					
				}
				,add_control_panel_button: function()
				{
					var thisA = this;
					var thisP = this.thisP;
					// control button
					var cp_btn = thisA.control_obj
					.append("text")
					.attr("class", "control_panel_text")
					.text("Control Panel")
					.style("text-anchor", "end");
					
					$(cp_btn[0][0]).bind("click", function(){
						$(thisA.control_panel_obj).toggle("slide", {direction: 'right'});
						thisA.control_panel_obj.html("");
						thisA.add_control_panel_topng();
						thisA.add_control_panel_y_range();
					});
				}
				,add_control_panel_main: function()
				{
					var thisA = this;
					thisA.control_panel_obj = $(thisA.control_obj[0][0])
					.parent("svg")
					.parent()
					.append("<div class='d3_control_panel' style='display:none;'></div>")
					.children()
					.last();
				}
				,add_control_panel_topng: function()
				{
					var thisA = this;
					var obj = thisA.control_panel_obj
					.append("<div class='d3_control_panel_item'></div>").children().last();
					
					obj.append("<span>ToPNG</span>")
					.bind("click", function(){
						thisA.toPNG();
						thisA.control_panel_obj.hide();
					});
					
					obj = thisA.control_panel_obj
					.append("<div class='d3_control_panel_item'></div>").children().last();
					
					obj.append("<span>ToPNG_HD</span>")
					.bind("click", function(){
						thisA.toPNG(true);
						thisA.control_panel_obj.hide();
					});
				}
				,add_control_panel_y_range: function()
				{
					var thisP = this.thisP;
					var thisA = this;
					
					if(! thisP.config.y0)
						return;
					
					var obj = thisA.control_panel_obj
					.append("<div class='d3_control_panel_item'></div>").children().last();
					
					var checked = "";
					if(thisP.config.fixed_y)
						checked = "checked='checked'";
					
					var y_min_max = thisP.config.y0.domain();
					obj.append("<span>Y-axis</span>")
					.append("Fixed Y <input type='checkbox' class='d3_y_is_fixed' "+checked+" ></input>~")
					.append("<input type='text' class='d3_y_axis_min' value='"+y_min_max[0]+"'></input>~")
					.append("<input type='text' class='d3_y_axis_max' value='"+y_min_max[1]+"'></input>");
					
					obj.find("input").bind("change", function(){
						var is_fixed = obj.find("input.d3_y_is_fixed").prop("checked");
						var new_y_min_max = [obj.find("input.d3_y_axis_min").val(), obj.find("input.d3_y_axis_max").val()];
						
						if(is_fixed)
							thisP.config.fixed_y = new_y_min_max;
						else
							thisP.config.fixed_y = false;
						thisP.redraw();
					});
				}
				/**
				 * @brief control 增加 "輸出圖片"事件功能，與移除 svg 事件功能
				 * @param null
				 * @return null
				 */
				/// !add_png_button
				,add_png_button : function()
				{
					var thisA = this;
					if(!thisA.control_obj) return;
					
					thisA.control_obj
					.append("text")
					.style("text-anchor", "end")
					.text("GetPNG")
					.attr("dy", ".35em")
					.on("click", function(){
						thisA.toPNG();
					})
					
					thisA.control_obj
					.append("text")
					.style("text-anchor", "end")
					.text("remove")
					.attr("x", -80)
					.attr("dy", ".35em")
					.on("click", function(){
						thisA.empty();
					})
				}
				
				/**
				 * @brief svg轉換png 實作 
				 * @param null
				 * @return null
				 */
				/// !toPNG
				,toPNG : function(is_hd)
				{
					var thisA =this;
					var thisP = this.thisP;
					var svg = thisP.config.draw_obj[0][0];
					$(thisA.control_obj[0][0]).hide();
					if(is_hd)
						$(svg).attr("height", "1200").attr("width", "2400");
					else
						$(svg).attr("height", "600").attr("width", "1200");
					var url = svg.toDataURL("image/png", {"renderer" : "canvg", "callback":function(data){
						window.open(data, "t","location=0,menubar=0,scrollbars=1,status=1,resizable=1");
					}});
					$(thisA.control_obj[0][0]).show();
					$(svg).attr("height", "100%").attr("width", "100%");
				}
				
				/**
				 * @brief svg 清空實作
				 * @param null
				 * @return null
				 */
				/// !empty
				,empty : function()
				{
					var thisP = this.thisP;
					var svg = thisP.config.draw_obj[0][0];
					$(svg).empty();
					thisA.config = {};
					thisA.data = {};
				}
				
				
				
			}
			
		);
	
	}
);
