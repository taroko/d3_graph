/**
 *  @file d3_graph/js/d3/d3_helper.js
 *  @brief 常用功能
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"d3_graph/js/d3.v3.min.js"
	],function(declare, lang, d3){
		return declare( "D3_strategy_axis", null,
			{
				make_axis: function(strategy_child)
				{
					for(var key in strategy_child)
					{
						switch(key)
						{
							case "x0":
								this.config.x0 = this.make_axis_xy_impl(strategy_child[key].domain, strategy_child[key].range, key);
								if(!this.config.xAxis)
									this.config.xAxis = d3.svg.axis().scale(this.config.x0).orient("bottom");
								this.config.xAxis.scale(this.config.x0);
								break;
							case "x1":
								this.config.x1 = this.make_axis_xy_impl(strategy_child[key].domain, strategy_child[key].range, key);
								break;
							case "y0":
								this.config.y0 = this.make_axis_xy_impl(strategy_child[key].domain, strategy_child[key].range, key);
								if(!this.config.yAxis)
									this.config.yAxis = d3.svg.axis().scale(this.config.y0).orient("left");
								this.config.yAxis.scale(this.config.y0);
								break;
							case "pie":
								this.config.pie = this.make_axis_pie_pie(strategy_child[key]);
								break;
							case "arc":
								this.config.arc = this.make_axis_pie_arc(strategy_child[key]);
								break;
						}
					}
					
				}
				,make_axis_pie_pie: function(strategy_child)
				{
					var thisA = this;
					var data_number = thisA.config.raw_keys.length;
					var obj_array = [];
					
					switch(strategy_child)
					{
						case "single_layer":
							data_number = 1;
						case "multi_layer":
							for(var i=0; i<data_number; i++)
							{
								var obj = d3.layout.pie();
								obj.sort(null).value(function(d) { return d[thisA.config.raw_key_2st]; });
								obj_array.push(obj);
							}
							break;
					}
					//console.log(obj_array);
					//this.config.pie = d3.layout.pie();
					return obj_array;
				}
				,make_axis_pie_arc: function(strategy_child)
				{
					var thisA = this;
					var data_number = thisA.config.raw_keys.length;
					var obj_array = [];
					
					switch(strategy_child)
					{
						case "single_layer":
							data_number = 1;
						case "multi_layer":
							var agergy_width = (thisA.config.radius-20) / data_number;
							if(agergy_width < 2)
							{
								agergy_width = 2;
							}
							for(var i=0; i<data_number; i++)
							{
								var obj = d3.svg.arc();
								obj
								.outerRadius(thisA.config.radius - 10 - i*agergy_width)
								.innerRadius(thisA.config.radius - 10 - (i+1)*agergy_width);
								obj_array.push(obj);
							}
							break;
					}
					//console.log(obj_array);
					//this.config.pie = d3.layout.pie();
					return obj_array;
				}
				,make_axis_xy_impl: function(domain, range, xy_type)
				{
					var thisA = this;
					var obj = {};

					//set domain
					switch(domain)
					{
						case "1st_column":
							obj = d3.scale.ordinal();
							obj.domain(thisA.data.map(function(d) { return d[thisA.config.raw_key_1st]; }));
							break;
						case "1st_raw":
							obj = d3.scale.ordinal();
							obj.domain(thisA.config.raw_keys);
							break;
						case "max_2st_column":
							obj = d3.scale.linear();
							obj.domain([0, thisA.config.min_max_2st[1]]);
							break;
						case "max_3st_column":
							obj = d3.scale.linear();
							obj.domain([0, thisA.config.min_max_3st[1]]);
							break;
						case "max_data_array_total":
							obj = d3.scale.linear();
							obj.domain([0, d3.max(thisA.data, function(d) { return d.total; }) ]);
							break;
						case "max_all":
							obj = d3.scale.linear();
							obj.domain([0, thisA.config.all_max]);
							break;
						case "normalized":
							obj = d3.scale.linear();
							break;
						case "min_max_2st_column":
							obj = d3.scale.linear();
							obj.domain(thisA.config.min_max_2st);
							break;
						case "min_max_3st_column":
							obj = d3.scale.linear();
							obj.domain(thisA.config.min_max_3st);
							break;
					}
					
					//linear and fixed
					if(typeof thisA.config.fixed_x == "object" && xy_type[0] == "x" && !obj.rangeBand)
					{
						obj.domain(thisA.config.fixed_x);
					}
					if(typeof thisA.config.fixed_y == "object" && xy_type[0] == "y" && !obj.rangeBand)
					{
						obj.domain(thisA.config.fixed_y);
					}
					
					///@brief set range
					var tmp_range = [];
					if(xy_type[0] == "x")
						tmp_range = [0, thisA.config.width_];
					else if(xy_type[0] == "y")
						tmp_range = [thisA.config.height_, 0];
					
					switch(range)
					{
						case "rangeRoundBands":
							if(xy_type == "x1")
								obj.rangeRoundBands([0, thisA.config.x0.rangeBand()]);
							else
								obj.rangeRoundBands(tmp_range, 0.1 );
							break;
						case "rangeRound":
								obj.rangeRound(tmp_range).nice();
							break;
						case "range":
								obj.range(tmp_range);
							break;
					}
					return obj;
				}
				,draw_axis: function(strategy_child)
				{
					var type = strategy_child.type;
					switch(type)
					{
						case "xy":
							this.draw_axis_xy(strategy_child);
							break;
					}
				}
				,draw_axis_xy: function(strategy_child)
				{
					for(var key in strategy_child)
					{
						this.draw_axis_xy_impl(key, strategy_child[key]);
					}
				}
				,draw_axis_xy_impl: function(axis_name, axis_type)
				{
					var thisA = this;
					var jWindow = thisA.config.window;
					var limit = 20;
					if(axis_type && axis_type.limit)
						limit = axis_type.limit;
					switch(axis_name)
					{
						case "x":
							// 最小3個，最大20個
							var total_num = thisA.config.column_keys.length;
							if(limit > total_num*0.7) limit = total_num;
							limit = Math.max(limit, 3);
							var tmp_keys = [];
							for(var i=0; i<limit; i++)
							{
								var idx = Math.floor(i*(total_num-1)/(limit-1));
								tmp_keys.push(thisA.config.column_keys[idx]);
							}
							thisA.config.xAxis.tickValues(tmp_keys);
							jWindow.append("g")
							.attr("class", "x_axis")
							.attr("transform", "translate(0," + (thisA.config.height_ ) + ")")
							.call(thisA.config.xAxis);
							thisA.x_axis_rotate();
							break;
						case "y":
							var total_num = 16;
							// 最小3個，最大16個
							if(limit > total_num) limit = total_num;
							limit = Math.max(limit, 3);
							thisA.config.yAxis.tickFormat(d3.format(".2s")).ticks(limit);
							// for normalize bar char
							if(thisA.config.y0.domain()[0] == 0 && thisA.config.y0.domain()[1] == 1)
								thisA.config.yAxis.tickFormat(d3.format(".0%"));
							jWindow.append("g")
							.attr("class", "y_axis")
							.attr("transform", "translate(6,0)")
							.call(thisA.config.yAxis)
							.append("text")
							.attr("transform", "rotate(-90)")
							.attr("y", 6)
							.attr("dy", ".71em")
							.style("text-anchor", "end");
					}
				}
				,redraw_axis: function(strategy_child)
				{
					var type = strategy_child.type;
					switch(type)
					{
						case "xy":
							this.redraw_axis_xy(strategy_child);
							break;
					}
				}
				,redraw_axis_xy: function(strategy_child)
				{
					for(var key in strategy_child)
					{
						this.redraw_axis_xy_impl(key, strategy_child[key]);
					}
				}
				,redraw_axis_xy_impl: function(axis_name, axis_type)
				{
					var thisA = this;
					var jWindow = thisA.config.window;
					
					switch(axis_name)
					{
						case "x":
							thisA.config.window.select("g.x_axis")
							.transition()
							.call(thisA.config.xAxis);
							break;
						case "y":
							//thisA.config.yAxis.tickFormat(d3.format(".3s"));
							//thisA.config.yAxis.tickFormat(thisA.config.yAxis.tickFormat());
							//thisA.config.yAxis.ticks(thisA.config.yAxis.ticks());
							thisA.config.window.select("g.y_axis")
							.transition()
							.call(thisA.config.yAxis);
							
							break;
					}
				}
			}
			
		);
	
	}
);
