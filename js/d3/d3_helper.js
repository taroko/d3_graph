/**
 *  @file d3_graph/js/d3/d3_helper.js
 *  @brief 常用功能
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"d3_graph/js/d3.v3.min.js"
		,"d3_graph/src/jquery.md5.js"
		,"d3_graph/src/jquery.tipsy.js"
		,"d3_graph/src/SVG.toDataURL/svg_todataurl.js"
		,"d3_graph/src/canvg/rgbcolor.js"
		,"d3_graph/src/canvg/canvg.js"
		,"d3_graph/src/jquery.ba-resize.js"
		
	],function(declare, lang, d3, md5, tipsy){
		return declare( "D3_helper", null,
			{
				constructor : function()
				{
					//console.log("D3_helper constructor");
				}
				/**
				 * @brief 讀取 tsv檔, 使用d3預設方法
				 * @param done 讀取完做的 callback
				 * @return promise, jquery 的管理方法, 可以使用 .done( callback ), 並且控制權不會被搶走
				 */
				/// !load_data
				,load_data : function(done)
				{
					var dtd = $.Deferred();
					d3.tsv(this.config.filename,
						lang.hitch(this, function(error, data){
							this.data = data;
							if(done) done(error, data);
							dtd.resolve(error,data);
						})
					);
					return dtd.promise();
				}
				
				/**
				 * @brief 遞迴merge物件
				 * @param configA 主要物件 (融合後最大)
				 * @param configB 被融合物件 (副)
				 * @return null
				 */
				/// !merge_config
				,merge_config : function(configA, configB)
				{
					for(var key in configB)
					{
						if(typeof(configA[key]) == "object" || typeof(configA[key]) == "array")
						{
							this.merge_config(configA[key], configB[key]);
						}
						else
						{
							configA[key] = configB[key];
						}
					}
				}
				
				/**
				 * @brief 取得 d3 data 物件順序為 idx 的 key name
				 * @param data d3 data 物件結構 ( data = d3.tsv(filename))
				 * @param idx 要取的物件順序
				 * @return key_name (string)
				 */
				/// !get_data_key_name
				,get_data_key_name : function(data, idx)
				{
					var i=0;
					var key_name;
					for(var key in data)
					{
						for(var key2 in data[key])
						{
							if(i == idx)
								return key2;
							i++;
						}
					}
				}
				,get_data_nst_raw_key: function(data, idx)
				{
					var i=0;
					var key_name;
					for(var key in data)
					{
						for(var key2 in data[key])
						{
							if(i == idx)
								return key2;
							i++;
						}
					}
				}
				,get_data_raw_keys: function(data)
				{
					var keys = [];
					for(var key in data)
					{
						for(var key2 in data[key])
						{
							keys.push(key2);
						}
						break;
					}
					return keys;
				}
				,get_data_column_keys: function(data)
				{
					var keys = [];
					for(var key in data)
					{
						for(var key2 in data[key])
						{
							keys.push(data[key][key2]);
							break;
						}
					}
					return keys;
				}
				/// !encode_records
				,encode_records : {}
				
				/**
				 * @brief string to md5 string
				 * @param text string
				 * @return md5 string
				 */
				/// !encode
				,encode : function(text)
				{
					var md5 = $.md5(text);
					if(!this.encode_records[md5])
						this.encode_records[md5] = text;
					return md5;
				}
				
				/**
				 * @brief 設定預設滑鼠事件，滑鼠移過同類變色紅色
				 * @param jWindow 要監聽事件的最大物件，jQuery window(圖層)物件，$(thisA.config.window[0])
				 * @return null
				 */
				/// !mouse_event_select_color
				,mouse_event_select_color : function(jWindow)
				{
					/// @brief 設定滑鼠移過去，相同類變紅色
					var thisA = this;
					
					jWindow.find(".item_class")
					.unbind("mouseover").bind("mouseover", function(){
						if(thisA.config.mouse_event_disable_change_color)
							return;
						var this_classes = $(this).attr("class").split(" ");
						var this_class = this_classes[this_classes.length - 1];
						if($(this).css("fill") == "#ff0000" ) return;
						$(this).data("color", $(this).css("fill"));							
						jWindow.find("." + this_class).css("fill", "#ff0000");
					}).unbind("mouseout").bind("mouseout", function(){
						if(thisA.config.mouse_event_disable_change_color)
							return;
						var this_classes = $(this).attr("class").split(" ");
						var this_class = this_classes[this_classes.length - 1];
						jWindow.find("." + this_class).css("fill", $(this).data("color"));
					});
				}
				,mouse_event_right_click: function()
				{
					var jWindow = $(this.config.window[0]);
					var thisA = this;
					
					// x-axis right click
					if(thisA.config.x_axis_right_click)
					{
						jWindow.find(".x_axis>g.tick").unbind("contextmenu").bind("contextmenu", function(e) {
							var x_axis_name = $(this).children("text").text();
							thisA.config.x_axis_right_click(e, x_axis_name, thisA);
						});
					}
					// legend right click
					if(thisA.config.legend_right_click)
					{
						jWindow.find(".legend").unbind("contextmenu").bind("contextmenu", function(e){
							var legend_name = $(this).children("text").text();
							thisA.config.legend_right_click(e, legend_name, thisA);
						});
					}
					
				}
				/**
				 * @brief 滑鼠移過呼叫 callback function
				 * @param jWindow 要監聽事件的最大物件，jQuery window(圖層)物件，$(thisA.config.window[0])
				 * @return null
				 */
				/// !mouse_event_callback_rowcol
				,mouse_event_callback_rowcol : function(jWindow)
				{
					var thisA = this;
					jWindow.find(".mouse_event")
					.unbind("mouseover").bind("mouseover", function()
					{
						if(thisA.config.cb_mouseover)
						{
							if($(this).parent().parent().find(".mouse_event_row").length != 0)
							{
								var row = $(this).parent().parent().find(".mouse_event_row").index($(this).parent());
								var column = $(this).parent().find(".mouse_event").index($(this));
							}
							else
							{
								var row = $(this).parent().find(".mouse_event").index($(this));
							}
							if(thisA.is_transposed)
								thisA.config.cb_mouseover(column, row);
							else
								thisA.config.cb_mouseover(row, column);
						}
					}).unbind("mouseout").bind("mouseout", function()
					{	
						if(thisA.config.cb_mouseout)
						{
							if($(this).parent().parent().find(".mouse_event_row").length != 0)
							{
								var row = $(this).parent().parent().find(".mouse_event_row").index($(this).parent());
								var column = $(this).parent().find(".mouse_event").index($(this));
							}
							else
							{
								var row = $(this).parent().find(".mouse_event").index($(this));
							}
							if(thisA.is_transposed)
								thisA.config.cb_mouseout(column, row);
							else
								thisA.config.cb_mouseout(row, column);
						}
					});
				}
				
				/**
				 * @brief 設定滑鼠事件，移過出現 title tips
				 * @param jWindow 要監聽事件的最大物件，jQuery window(圖層)物件，$(thisA.config.window[0])
				 * @return null
				 */
				/// !mouse_event_tipsy
				,mouse_event_tipsy : function(jWindow)
				{
					$(document).find(".tipsy").remove();
					if(jWindow.find(".item_class").tipsy)
						jWindow.find(".item_class").tipsy({gravity: 's'});
				}
				,transpose: function(data)
				{
					/*
					[{idx:"15", noseq:10}, {idx:"16", noseq:20}, {idx:"17", noseq:15}]
					TO
					[{idx:"noseq", "15":10, "16":20, "17":15}]
					*/
					var raw_key = this.get_data_raw_keys(data); // "idx", "noseq"
					var column_keys = this.get_data_column_keys(data); //"15", "16", "17"
					
					var arr = [];
					for(var ridx in data)
					{
						var arr_ri = 0;
						for(var cidx in data[ridx])
						{
							if(cidx == raw_key[0])
								continue;
							
							if(ridx == 0)
							{
								arr.push({});
								arr[arr_ri][ raw_key[0] ] = cidx; 
							}
							arr[arr_ri][ data[ridx][ raw_key[0]]+"\0" ] = +data[ridx][cidx];
							arr_ri++;
						}
					}
					return arr;
				}
				,data_copy: function(from, to)
				{
					to.splice(0, to.length)
					for(var key in from)
					{
						var obj = {};
						$.extend(obj, from[key]);
						to.push(obj);
					}
					//to = JSON.parse(JSON.stringify(from));
				}
				,recover_data: function()
				{
					var thisA = this;
					// 還原原本數據
					//thisA.data_copy(thisA.origin_data, thisA.data);
					thisA.data.forEach(function(d, i) 
					{
						for(var key in thisA.hide_data.column)
						{
							d[key] = thisA.origin_data[i][key];
						}
					});
					thisA.data.forEach(function(d, i) 
					{
						for(var key in thisA.hide_data.raw)
						{
							if(d[thisA.config.raw_key_1st] != key)
			    				continue;
			    			for(var d_key in d)
							{
								d[d_key] = thisA.origin_data[i][d_key];
							}
						}
					});
				}
				,redraw_callback: function()
				{
					///@brief callback axis 刪除線重劃
					var thisA = this;
					var jWindow = $(thisA.config.window[0]);
					
					jWindow.find(".x_axis>g.tick text, .legend text").css("text-decoration", "none").each(function(){
						for(var key in thisA.hide_data.raw)
						{
							if($(this).html() == key)
								$(this).css("text-decoration", "line-through");
						}
					});
					jWindow.find(".legend text").css("text-decoration", "none").each(function(){
						for(var key in thisA.hide_data.column)
						{
							if($(this).html() == key)
								$(this).css("text-decoration", "line-through");
						}
					});
				}
				,legend_axis_hide_data_impl: function()
				{
					var thisA = this;
					// 隱藏 legend
					thisA.data.forEach(function(d) 
					{
						for(var key in thisA.hide_data.column)
						{
							d[key] = 0;
						}
					});
					// 隱藏 x-axis
					thisA.data.forEach(function(d, i) 
					{
						for(var key in thisA.hide_data.raw)
						{
							if(d[thisA.config.raw_key_1st] != key)
			    				continue;
			    			for(var d_key in d)
							{
								if(d_key != thisA.config.raw_key_1st)
									d[d_key] = 0;
							}
						}
					});
				}
				,legend_axis_hide_data_redraw: function(jWindow, raw_column, key_name)
				{
					var thisA = this;
					var is_hide = false;
					
					if(thisA.hide_data[raw_column][key_name]) is_hide = true;
					console.log(raw_column, key_name, is_hide, thisA.data);
					
					///@brief 確認要隱藏還是顯示，並且做出設定

					//目前是隱藏狀態，要顯示
					if(is_hide)
				    {
				    	thisA.recover_data();
				    	delete thisA.hide_data[raw_column][key_name];
				    }
				    //目前是顯示狀態，要隱藏
				    else
				    {
				    	thisA.recover_data();
				    	thisA.hide_data[raw_column][key_name] = true;
				    }
					
					///@brief 對data做修改，把目標直改成0，達到hide目的
					thisA.legend_axis_hide_data_impl();
					
					thisA.redraw();
				}
				,mouse_event_legend_click : function(jWindow)
				{
					var thisA = this;
					jWindow.find(".legend").unbind("click").bind("click", function(){
						var legend_name = $(this).children("text").text();
						thisA.legend_axis_hide_data_redraw(jWindow, "column", legend_name);
					});
				}
				,mouse_event_x_axis_click : function(jWindow)
				{
					var thisA = this;
					jWindow.find(".x_axis>g.tick").unbind("click").bind("click", function() {
						var x_axis_name = $(this).children("text").text();
						thisA.legend_axis_hide_data_redraw(jWindow, "raw", x_axis_name);
					});
				}
				,reset_data: function(data)
				{
					this.data = data;
					this.data_copy(this.data, this.origin_data);
					//把原本隱藏的 data 繼續隱藏
					this.legend_axis_hide_data_impl();
					this.calculate_basic_parameter();
				}
				///@brief 圖案完全重劃
				,redraw: function(data, type)
				{
					var jWindow = $(this.config.window[0]);
					var thisA = this;
					
					if(data) thisA.reset_data(data);
					
					///@brief 如果有定義重劃機制（可能會比較華麗，有動畫），就使用 redraw_soft
					if(thisA.strategy["redraw"] && type != "all")
					{
						// redraw soft 成功的話就離開
						if(!thisA.redraw_soft(data))
							return;
					}
					
					jWindow.find("g").remove();
					
					// if no data, draw No data tips
					if(this.draw_nodata()) return;
					
					//jWindow.next().children().remove();
					thisA.data.forEach(function(d) {
					    delete d.data_array;
					    delete d.total;
					});
					// old version
					//thisA.draw(thisA.data);
					thisA.make(thisA.strategy);
					
					jWindow.parent("svg").trigger("resize");
					
					// 目前主要是要恢復legend刪除線
					thisA.redraw_callback();
				}
				///@brief 如果有定義重劃機制（可能會比較華麗，有動畫），就使用 redraw_soft
				,redraw_soft: function(data)
				{
					var jWindow = $(this.config.window[0]);
					var thisA = this;
					
					//if(data) thisA.reset_data(data);
					
					// if no data, draw No data tips
					if(thisA.draw_nodata())
						return false;
					
					//jWindow.find("g").remove();
					//jWindow.next().children().remove();
					thisA.data.forEach(function(d) {
					    delete d.data_array;
					    delete d.total;
					});
					var strategy = thisA.strategy;
					
					//if(strategy["color_domain"])
					//	this.color_domain(strategy["color_domain"]);
					if(strategy["data_handl"])
						this.data_handl(strategy["data_handl"]);
					if(strategy["make_axis"])
						this.make_axis(strategy["make_axis"]);
					//if(strategy["draw_axis"])
					//	this.draw_axis(strategy["draw_axis"]);
					//if(strategy["draw_content"])
					//	this.draw_content(strategy["draw_content"]);
					//if(strategy["draw_legend"])
					//	this.draw_legend(strategy["draw_legend"]);
					//if(strategy["redraw_content"])
					//	this.redraw_content(strategy["redraw_content"]);
					
					var strategy_redraw = strategy["redraw"];
					if(strategy_redraw["redraw_axis"])
						this.redraw_axis(strategy_redraw["redraw_axis"]);
					if(strategy_redraw["redraw_content"])
						this.redraw_content(strategy_redraw["redraw_content"]);
					
					// 目前主要是要恢復legend刪除線
					thisA.redraw_callback();
					return true;
				}
				,resize_words: function()
				{
					//DOMSubtreeModified
					var thisA = this;
					var jWindow = $(this.config.window[0]);
					var jSvg = jWindow.parent("svg");
					
					if($(jSvg).data("isBindResize"))
						return;
					
					// using jquery.ba-resize.js
					$(jSvg).data("isBindResize", true).bind("resize", function(){
						
						if(thisA.draw_nodata()) return;
						
						var each_legend_height = 20;
						var each_x_axis_width = 20;
						
						//重新計算 legend 長度，避免字擠在一起
						var svg_width = $(thisA.config.window[0]).parent().width();
						var svg_height = $(thisA.config.window[0]).parent().height();
						svg_height -= (thisA.config.margin.top + thisA.config.margin.bottom - 80) * (svg_height/600);
						svg_width -= (thisA.config.margin.right + thisA.config.margin.left + 100) * (svg_width/1200);
						if(svg_height > svg_width*0.6)	svg_height = svg_width*0.6;
						if(svg_width > svg_height*1.67)	svg_width = svg_height*1.67;
						
						var legend_num = Math.floor( svg_height / each_legend_height);
						var x_axis_num = Math.floor( svg_width / each_x_axis_width);
						
						var jWindow = $(thisA.config.window[0]);
						
						// legend list number limit
						if(thisA.strategy["draw_legend"])
						{
							var obj = thisA.strategy["draw_legend"];
							obj.limit = legend_num;
							jWindow.find(".legends").remove();
							thisA.draw_legend(obj);
						}
						// x-axis show items number limit
						if(thisA.strategy["draw_axis"] && thisA.strategy["draw_axis"]["x"])
						{
							var obj = thisA.strategy["draw_axis"];
							obj.x["limit"] = x_axis_num;
							jWindow.find(".x_axis").remove();
							thisA.draw_axis(obj);
						}
						// y-axis show item number limit
						if(thisA.strategy["draw_axis"] && thisA.strategy["draw_axis"]["y"])
						{
							var obj = thisA.strategy["draw_axis"];
							obj.y["limit"] = legend_num;
							jWindow.find(".y_axis").remove();
							thisA.draw_axis(obj);
						}
						
						//console.log("resize_legend", legend_num, x_axis_num, svg_height, svg_width);
						
						// 目前主要是要恢復legend刪除線
						thisA.redraw_callback();
						
						// 因為重劃，所以事件會消失
						thisA.mouse_event();
					});
					// 剛開始畫出來就跑一次 on resize
					$(jSvg).trigger("resize");
				}
				/**
				 * @brief 設定預設滑鼠事件，滑鼠移過同類變色紅色，滑鼠移過呼叫 callback function
				 * @param jWindow 要監聽事件的最大物件，jQuery window(圖層)物件，$(thisA.config.window[0])
				 * @return null
				 */
				/// !mouse_event
				,mouse_event : function()
				{
					var thisA = this;
					var jWindow = $(thisA.config.window[0]);
					
					/// @brief 設定滑鼠移過去，相同類變紅色
					thisA.mouse_event_select_color(jWindow);
					
					/// @brief 設定滑鼠移過，的 callback function，主要給予 data 是第幾 row, col
					thisA.mouse_event_callback_rowcol(jWindow);
					
					/// @brief 滑鼠移過，出現title提示
					thisA.mouse_event_tipsy(jWindow)
					
					/// @brief 滑鼠點擊 Legend
					thisA.mouse_event_legend_click(jWindow);
					
					/// @brief 滑鼠點擊 X axis
					thisA.mouse_event_x_axis_click(jWindow);
					
					/// @bieif 滑鼠右鍵點擊 X axis, Legend
					thisA.mouse_event_right_click();
					
					thisA.resize_words();
				}
				
				/**
				 * @brief 重新調整（計算）data中的值，可以取log等，最重要的是，從檔案讀取的 value 可能會誤判為字串，這邊轉 int
				 * @param keys d3 data 中要修改 keys (array)
				 * @param cal 計算 function，輸入為data原始值，回傳為計算過後的值
				 * @return null
				 */
				/// !adjust_data
				,adjust_data : function(keys, cal)
				{
					var thisA = this;
					if(!cal)
					{
						cal = function(d){return d;};
						if(this.config.tolog)
							cal = function(d){return Math.log(d) / Math.log(thisA.config.tolog);}
					}
					this.data.forEach(function(d){
						for(var key in keys)
						{
							d[ keys[key] ] = + cal( +d[ keys[key] ]).toFixed(3);
						}
					});
				}
				,check_is_nodata: function()
				{
					if(this.data.length == 0)
						return true;
					for(var key in this.config.raw_keys)
					{
						if(this.data[0][this.config.raw_keys[key]] == undefined)
							return true;
					}
					return false;
				}
				/**
				 * @brief x 軸，text 會擠滿互相覆蓋，因此轉個角度，可以不會互相覆蓋
				 * @param null
				 * @return null
				 */
				/// !x_axis_rotate
				,x_axis_rotate : function()
				{
					this.config.window
					.selectAll(".x_axis")
					.selectAll(".tick")
					.selectAll("text")
					.style("text-anchor", "start")
					.attr("transform", "translate(0,14), rotate(45)");
				}
			}
			
		);
	
	}
);
