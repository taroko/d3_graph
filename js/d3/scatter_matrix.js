/**
 *  @file Jhhd3_graph/js/d3/scattermatrix.js
 *  @brief 多維 scatter plot
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
		,"d3_graph/js/d3/scatterchart"
	],function(declare, lang, d3, D3_helper, scatterchart){
		return declare( "ScatterMatrix", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
					//console.log("D3 ScatterChart constructor");
				}
				
				/**
				 * @brief 讀檔畫圖，如果 data 沒有設定，則讀檔
				 * @param data d3 data object
				 * @return null
				 */
				/// !draw
				,draw : function(data)
				{
				
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
				}
				
				/**
				 * @brief 畫圖實作，x軸 y軸 內容 圖標
				 * @param error Error callback, not impl
				 * @param data d3 data object
				 * @return function 
				 */
				/// !draw_impl
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data)
					{
						var thisA = this;
						this.data = data;
						
						thisA._setup_xy();
						
						var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
						var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
						
						/// @brief svg using viewbox, 實際螢幕大小與設定的大小會因為縮放而不同，因此在取得 mouse xy 時會有誤差，此 ratio 是為了修正誤差
						var m_ratio = Math.min( (thisA.config.draw_obj[0][0].scrollHeight / thisA.config.height), (thisA.config.draw_obj[0][0].scrollWidth/thisA.config.width));
						
						thisA.config.cell = [];
						/// @brief 迴圈 data 維度的平方次
						for(var i=0; i<data_key_names.length; ++i)
						{
							thisA.config.cell[i] = [];
							for(var j=0; j<data_key_names.length; ++j)
							{
								thisA.config.cell[i][j] = {};
								
								/// @brief 每一隔小 scatter plot 都是從原始"圖層"，新增 <g></g>後，在畫圖，此圖層稱為 sub_window
								thisA.config.cell[i][j].sub_window = thisA.config.window
								.append("g")
								.attr("class","cell")
								.attr("transform", "translate("+ thisA.config.x0(data_key_names[i]) +"," + thisA.config.y0(data_key_names[j]) + ")");
								
								/// @brief 在 sub_window <g> 下新增 <rect> 作為之後 mouse listen event用
								thisA.config.cell[i][j].sub_window
								.append("rect")
								.attr("width", thisA.config.width_/data_key_names.length - 12)
								.attr("height", thisA.config.height_/data_key_names.length - 12)
								.attr("fill", "#eee")
								.attr("stroke", "#aaa");
								
								/// @brief 每個維度都創建 scatter plot，為 sub_obj
								thisA.config.cell[i][j].sub_obj = new D3(
								{
									"tolog":false,
									"width":thisA.config.width_/data_key_names.length - 12, 
									"height" :thisA.config.height_/data_key_names.length - 12, 
									"margin" : {"left":0, "top":0, "right":0, "bottom":0}
								});
								
								/// @brief 模擬 D3::draw 所做的事情，創建 ScatterChart，設定 window，給予 data
								thisA.config.cell[i][j].sub_graph = new ScatterChart(thisA.config.cell[i][j].sub_obj.config);
								thisA.config.cell[i][j].sub_graph.config.window = thisA.config.cell[i][j].sub_window;
								thisA.config.cell[i][j].sub_graph.data = thisA.data;
								thisA.config.cell[i][j].sub_graph.config.r = 2;
								
								/// @brief 模擬 ScatterChart::draw_impl 所做的事情，設定座標軸，畫上 main content。沒有畫 xy axis
								thisA.config.cell[i][j].sub_graph._setup_xy(data_1st_key_name, data_key_names[i], data_key_names[j]);
								thisA.config.cell[i][j].sub_graph.draw_rect(data_1st_key_name, data_key_names[i], data_key_names[j]);
								
								/// @brief jQuery 選取 <g>
								thisA.config.cell[i][j].j_sub_window = $(thisA.config.cell[i][j].sub_window[0][0]);								
								
								/// @brief 設定小圖層滑鼠事件
								thisA.config.cell[i][j].j_sub_window
								.data("i", i)
								.data("j", j)
								.bind("dblclick", function(e){
									thisA.sub_window_event_dblclick(this, e, data_1st_key_name, data_key_names, m_ratio);
								})
								.bind("mousedown", function(e){
									thisA.sub_window_event_mousedown(this, e, data_1st_key_name, data_key_names, m_ratio);
								})
								.bind("mousemove", function(e){
									thisA.sub_window_event_mousemove(this, e, data_1st_key_name, data_key_names, m_ratio);
								})
								.bind("mouseup", function(e){
									thisA.sub_window_event_mouseup(this, e, data_1st_key_name, data_key_names, m_ratio);
								})
								
								/// @brief draw y axis
								if(i == 0)
								{
									thisA.config.cell[i][j].sub_graph.config.yAxis.ticks(5);
									thisA.config.cell[i][j].sub_graph.draw_yaxis(data_key_names[j]);
									
									thisA.config.cell[i][j].sub_window
									.selectAll(".tick")
									.selectAll("text")
									.style("font-size", "10px");
								}
								
								/// @brief draw x axis
								if(j == data_key_names.length-1)
								{
									thisA.config.cell[i][j].sub_graph.config.xAxis.ticks(6);
									thisA.config.cell[i][j].sub_graph.draw_xaxis(data_key_names[i]);
									thisA.config.cell[i][j].sub_graph.x_axis_rotate();
									
									thisA.config.cell[i][j].sub_window
									.selectAll(".tick")
									.selectAll("text")
									.style("font-size", "10px");
								}
							}
						}
						
						thisA.append_legend();
						
						thisA.add_png_button();
						
						thisA.mouse_event($(thisA.config.window[0]));
						
					});
				}
				
				/**
				 * @brief sub_window dbclick event listen, 從小圖轉成大圖
				 * @param thisB jQuery sub_window 物件， $.(sub_window)
				 * @param e event obj, include mouse xy
				 * @param data_1st_key_name data 第一欄位的 name (value)
				 * @param data_key_names data 第一行的所有 name (不包含 第一欄位)
				 * @param m_ration 實際螢幕大小與設定的大小會因為縮放而不同，因此在取得 mouse xy 時會有誤差，此 ratio 是為了修正誤差
				 * @return null
				 */
				/// !sub_window_event_dblclick
				,sub_window_event_dblclick : function(thisB, e, data_1st_key_name, data_key_names, m_ratio)
				{
					var thisA = this;
					
					/// @brief 取得觸發事件的物件 index i,j，可以正確取得物件
					var idx_i = $(thisB).data("i");
					var idx_j = $(thisB).data("j");
					
					/// @brief 複製一份 dbclick 的物件
					$(thisA.config.window[0][0]).append( $(thisA.config.cell[idx_i][idx_j].sub_graph.config.window[0][0]).clone() );
					
					/// @brief 選取剛剛複製出來的物件，並且移除座標軸
					var tmp_obj = $(thisA.config.window[0][0]).find(".cell").last();
					tmp_obj.addClass("tmp_obj");
					tmp_obj.find(".y_axis, .x_axis").remove();
					
					/// @brief 將dbclick的物件( 與複製的相同 )的座標複製起來
					var transform = tmp_obj.attr("transform");
					
					/// @brief 設定動畫，將剛剛複製的物件放大到 window 大小，然後移除，此時真正的物件已經做好了，所以再將真的物件(ScatterChart)顯示
					thisA.config.window
					.selectAll(".tmp_obj")
					.transition()
					.duration(400)
					.attr("transform", "translate(20,20), scale("+ data_key_names.length +")")
					.remove()
					.each('end', function(){
						/// @brief 將所有小物件隱藏起來
						thisA.config.window
						.selectAll(".cell")
						.attr("style", "display:none;");
						/// @brief 將真的大物件(ScatterChart)顯示
						thisA.config.window
						.selectAll(".main_window")
						.attr("style", "display:inline;");
					});
					
					/// @brief 創建一個 window 來放 "放大的物件"，並且先隱藏起來，等動畫完了再顯示
					thisA.config.cell[idx_i][idx_j].main_window = thisA.config.window
					.append("g")
					.attr("class","main_window")
					.attr("style", "display:none;");
					
					/// @brief 創建放大的物件 ScatterChart
					thisA.config.cell[idx_i][idx_j].main_obj = new D3();
					thisA.config.cell[idx_i][idx_j].main_graph = new ScatterChart(thisA.config.cell[idx_i][idx_j].main_obj.config);
				
					thisA.config.cell[idx_i][idx_j].main_graph.config.window = thisA.config.cell[idx_i][idx_j].main_window;
					thisA.config.cell[idx_i][idx_j].main_graph.config.r = 5;
					
					var draw_impl = thisA.config.cell[idx_i][idx_j].main_graph.draw_impl();
					draw_impl(null, thisA.data, data_1st_key_name, data_key_names[idx_i], data_key_names[idx_j]);
					
					/// @brief 設定"放大的物件" dbclick事件，將要縮小回去，並且移除，然後顯示所有小物件
					$(thisA.config.cell[idx_i][idx_j].main_graph.config.window[0][0])
					.bind("dblclick",function(){
						/// @brief 動畫縮小 "大物件"，然後移除
						thisA.config.window
						.selectAll(".main_window")
						.transition()
						.duration(400)
						.attr("transform", transform + ", scale("+ 1/data_key_names.length +")")
						.remove();
						/// @brief 顯示所有小物件
						thisA.config.window
						.selectAll(".cell")
						.attr("style", "display:inline;");
					});
				}
				
				/**
				 * @brief sub_window mousedown event listen, 選取範圍開始
				 * @param 上面已有註解
				 * @return null
				 */
				/// !sub_window_event_mousedown
				,sub_window_event_mousedown : function(thisB, e, data_1st_key_name, data_key_names, m_ratio)
				{
					var thisA = this;
					if($(thisB).data("mousedown"))
						return;
					
					/// @brief 停止當滑鼠移動到"點"時，改變顏色的功能，避免與 selection 衝突
					thisA.config.mouse_event_disable_change_color = true;
					
					/// @brief 重新設定 m_ration，當使用者改變視窗大小時，更新 m_ration
					m_ratio = Math.min( 
						(thisA.config.draw_obj[0][0].scrollHeight/thisA.config.height), 
						(thisA.config.draw_obj[0][0].scrollWidth/thisA.config.width)
					);
					
					/// @brief 要重新選取，所以先把舊的移除
					thisA.config.window
					.selectAll(".selection")
					.remove();
					
					/// @brief 取得觸發事件的物件 index i,j，可以正確取得物件
					var idx_i = $(thisB).data("i");
					var idx_j = $(thisB).data("j");
					
					/// @brief 取得小圖的 top and left
					var thisOffset = $(thisB).find("svg").offset();
					
					/// @brief 現在滑鼠在小圖中真實的座標
					var m_x = (e.pageX - thisOffset.left)/m_ratio;
					var m_y = (e.pageY - thisOffset.top)/m_ratio;
					
					/// @brief 創建 selection, 與設定大小位置
					thisA.config.cell[idx_i][idx_j].sub_window
					.append("rect")
					.attr("class", "selection")
					.attr("height", 10)
					.attr("width", 10)
					.style("fill-opacity", "0.2");
					
					$(thisB).find(".selection")
					.attr("x", m_x)
					.attr("y", m_y)
					.attr("height", 0)
					.attr("width", 0);
					
					/// @brief 記錄滑鼠mousedown的 xy，與設定 mousedown 事件開始
					$(thisB).data("mousedown_xy", [m_x, m_y]);
					$(thisB).data("mousedown", true);
				}
				
				/**
				 * @brief sub_window mousemove event listen, 選取範圍的過程，要調整控制 selection 物件大小，與磁性套鎖，離邊界太近，自動選取到邊界
				 * @param 上面已有註解
				 * @return null
				 */
				/// !sub_window_event_mousemove
				,sub_window_event_mousemove : function(thisB, e, data_1st_key_name, data_key_names, m_ratio)
				{
					var thisA = this;
					if(!$(thisB).data("mousedown"))
						return;
					
					/// @brief 取得觸發事件的物件 index i,j，可以正確取得物件
					var idx_i = $(thisB).data("i");
					var idx_j = $(thisB).data("j");	
					
					/// @brief 取得小圖的 top and left
					var thisOffset = $(thisB).find("svg").offset();
					
					/// @brief 取得mousedown時的 xy座標
					var mousedown_xy = $(thisB).data("mousedown_xy");
					
					/// @brief 現在滑鼠在小圖中真實的座標
					var m_x = (e.pageX - thisOffset.left)/m_ratio;
					var m_y = (e.pageY - thisOffset.top)/m_ratio;
					
					/// @brief selection 寬高
					var w = m_x - mousedown_xy[0];
					var h = m_y - mousedown_xy[1];
					
					var selection = $(thisB).find(".selection");
					
					/// @brief 當滑鼠移動到 mousedown xy 之上或之左，w or h < 0，必須修正
					if(w < 0)
					{
						selection.attr("x", m_x);
						w = -w;
					}
					if(h < 0)
					{
						selection.attr("y", m_y);
						h = -h;
					}
					
					/// @brief 即時調整selection 寬高
					selection
					.attr("width", w)
					.attr("height", h);
					
					/// @brief 取得小圖的寬高
					w = +$(thisB).find("rect").attr("width");
					h = +$(thisB).find("rect").attr("height");
					
					/// @brief 磁性套鎖，滑鼠離邊界很近，就自動選取到邊界
					if(m_x < 8 || m_y < 8 || m_x > w-8 || m_y > h-8)
					{
						if(m_x < 12)
						{
							selection
							.attr("x", 0)
							.attr("width", +selection.attr("width") + m_x);
						}
						if(m_y < 12)
						{
							selection
							.attr("y", 0)
							.attr("height", +selection.attr("height") + m_y);
						}
						if(m_x > w-12) selection.attr("width", w - mousedown_xy[0]);
						if(m_y > h-12) selection.attr("height", h - mousedown_xy[1]);
						$(thisB).trigger("mouseup");
					}
				}
				
				/**
				 * @brief sub_window mouseup event listen, 選取範圍結束，要判斷哪些又被選中，所有小圖同一資料，都要改顏色
				 * @param 上面已有註解
				 * @return null
				 */
				/// !sub_window_event_mouseup
				,sub_window_event_mouseup : function(thisB, e, data_1st_key_name, data_key_names, m_ratio)
				{
					var thisA = this;
					if(!$(thisB).data("mousedown"))
						return;
					
					/// @brief 設定 mousedown 事件結束
					$(thisB).data("mousedown", false);
					
					/// @brief 取得觸發事件的物件 index i,j，可以正確取得物件
					var idx_i = $(thisB).data("i");
					var idx_j = $(thisB).data("j");	
					
					var selection = $(thisB).find(".selection");
					
					/// @brief 取得selection 的 x,y 的邊界座標
					var limitX = [+selection.attr("x"), +selection.attr("x") + +selection.attr("width")];
					var limitY = [+selection.attr("y"), +selection.attr("y") + +selection.attr("height")];
					
					var jWindow = $(thisA.config.window[0][0]);
					
					var selection_number = 0;
					for(var ii =0; ii < thisA.data.length; ++ii)
					{
						/// @brief 迴圈所有data，將value轉成小圖座標 pixel
						var c_x = thisA.config.cell[idx_i][idx_j].sub_graph.config.x0(+thisA.data[ii][ data_key_names[idx_i] ]);
						var c_y = thisA.config.cell[idx_i][idx_j].sub_graph.config.y0(+thisA.data[ii][ data_key_names[idx_j] ]); 
						
						/// @brief 檢查是否有在selction範圍內
						if(c_x >= limitX[0] && c_x <= limitX[1] && c_y >= limitY[0] && c_y <= limitY[1])
						{
							if(thisA.config.cb_selected)
							{
								thisA.config.cb_selected(ii);
							}
							/// @brief 設定選取範圍內改變顏色
							selection_number++;
							jWindow.find(".item_order_" + thisA.encode(thisA.data[ii][ data_1st_key_name ] + ii) )
							.each(function(){
								if($(this).css("fill") == "#ff0000")
									return;
								$(this)
								.data("color", $(this).css("fill"))
								.css("fill", "#ff0000");
							});
						}
						else
						{
							if(thisA.config.cb_unselected)
							{
								thisA.config.cb_unselected(ii);
							}
							/// @brief 設定選取範圍內恢復顏色
							jWindow.find(".item_order_" + thisA.encode(thisA.data[ii][ data_1st_key_name ] + ii) )
							.each(function(){
								if(!$(this).data("color"))
									return;
								$(this)
								.css("fill", $(this).data("color"));
							});
						}
					}
					/// @brief 如果選取範圍內沒有選取到物件，則取消 mouse event 鎖定，讓滑鼠移過改變顏色功能恢復
					if(selection_number == 0)
					{
						thisA.config.mouse_event_disable_change_color = false;
					}
				}
				/**
				 * @brief 畫出 legend
				 * @param null
				 * @return null
				 */
				/// !append_legend
				,append_legend : function()
				{
					var thisA = this;
					var color_domain = thisA.config.cell[0][0].sub_graph.config.color.domain();
					
					var legend = thisA.config.window.selectAll(".legend")
					.data(color_domain)
					.enter().append("g")
					.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
					.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; })
					.style("fill", function(d){return thisA.config.color(d)});
					
					legend.append("rect")
					//.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
					.attr("x", thisA.config.width_ - 18 +20)
					.attr("width", 18)
					.attr("height", 18)
					.style("fill", thisA.config.color);
					
					legend.append("text")
					.attr("x", thisA.config.width_ - 24 + 48)
					.attr("y", 9)
					.attr("dy", ".35em")
					.style("text-anchor", "start")
					.text(function(d) { return d; });
				}
				
				/**
				 * @brief 設定座標軸的 domain (value max min), range (螢幕位置寬高)
				 * @param null
				 * @return null
				 */
				/// !_setup_xy
				,_setup_xy : function()
				{
					var thisA = this;
					
					var data_1st_key_name = this.get_data_key_name(this.data, 0);
					var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
					
					thisA.config.x0 = d3.scale.ordinal();
					thisA.config.y0 = d3.scale.ordinal();
					
					thisA.config.x0
					.domain(data_key_names)
					.rangeRoundBands([0, thisA.config.width_], 0.1 );
					
					thisA.config.y0
					.domain(data_key_names)
					.rangeRoundBands([0, thisA.config.height_], 0.1 );
					
					thisA.config.xAxis = d3.svg.axis().scale(thisA.config.x0).orient("bottom");
					thisA.config.yAxis = d3.svg.axis().scale(thisA.config.y0).orient("left");
					
				}
			}
			
		);
	
	}
);
