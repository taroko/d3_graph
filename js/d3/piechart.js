//d3_graph/js/d3/barchar.js

define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
	],function(declare, lang, d3, D3_helper){
		return declare( "PieChart", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
					//console.log("D3 BarChar constructor");
				}
				,draw : function(data)
				{
				
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
	
				}
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data)
					{
						var thisA = this;
						this.data = data;
						
						var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
						var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
						
						this.adjust_data([data_2st_key_name]);
						
						this._setup_xy();

						var g = thisA.config.window
						.append("g").attr("class", "main")
						.selectAll(".arc")
						.data(thisA.config.pie(data))
						.enter().append("g")
						.attr("class", "arc mouse_event");
						
						g.append("path")
						.attr("d", thisA.config.arc)
						.attr("class", function(d){ return "item_class item_class_" + thisA.encode(d.data[data_1st_key_name]);})
						.style("fill", function(d) { return thisA.config.color(d.data[data_1st_key_name]); })
						.attr("title", function(d) { return d.data[data_2st_key_name]; });
						
						g.append("text")
						.attr("transform", function(d) { return "translate(" + thisA.config.arc.centroid(d) + ")"; })
						.attr("dy", ".35em")
						.style("text-anchor", "middle")
						.text(function(d) { return d.data[data_1st_key_name]; });
						
						thisA.append_legend(data, data_1st_key_name);
						
						
						thisA.mouse_event($(thisA.config.window[0]));
						thisA.add_png_button();
					});
				}
				,append_legend : function(data, data_1st_key_name)
				{
					var thisA = this;
					var legend = thisA.config.window.selectAll(".legend")
						.data(data)
						.enter().append("g")
						.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d[data_1st_key_name]);})
						.attr("transform", function(d, i) { return "translate("+ (Math.floor(i/(thisA.config.radius/10))*140 + thisA.config.radius) + "," + ((i% Math.ceil(thisA.config.radius/10)) * 20 - thisA.config.radius) + ")"; })
						.style("fill", function(d){ return thisA.config.color(d[data_1st_key_name]); });
						
						legend.append("rect")
						.attr("x", 150 - 18 -20)
						.attr("width", 18)
						.attr("height", 18)
						.style("fill", function(d) { return thisA.config.color(d[data_1st_key_name]); });
						
						legend.append("text")
						.attr("x", 150 - 24 - 48)
						.attr("y", 9)
						.attr("dy", ".35em")
						.style("text-anchor", "start")
						.text(function(d) { return d[data_1st_key_name]; });
				}
				,_setup_xy : function()
				{
					var thisA = this;
					
					var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
					
					thisA.config.pie
					.sort(null)
					.value(function(d) { return d[data_2st_key_name]; });
					
					thisA.config.arc
					.outerRadius(thisA.config.radius - 10)
					.innerRadius(thisA.config.radius/4);
					
					thisA.config.window
					.attr("transform", "translate(" + thisA.config.center + "," + thisA.config.height / 2 + ")");
				}
			}
			
		);
	
	}
);
