/**
 *  @file Jhhd3_graph/js/d3/scattermatrix.js
 *  @brief xy plot with zoom
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"//d3js.org/d3.v3.min.js"
		,"d3_graph/js/d3/d3_helper"
	],function(declare, lang, d3, D3_helper){
		return declare( "ScatterChart", [D3_helper],
			{
				constructor : function(config)
				{
					this.config = config;
					//console.log("D3 ScatterChart constructor");
				}
				,draw : function(data)
				{
					if(!data)
					{
						d3.tsv(this.config.filename, this.draw_impl());
					}
					else
					{
						var draw_impl = this.draw_impl();
						draw_impl(null, data);
					}
				}
				,draw_impl : function()
				{
					return lang.hitch(this, function(error, data, data_1st_key_name, data_2st_key_name, data_3st_key_name)
					{
						var thisA = this;
						this.data = data;
						
						if(!data_1st_key_name)
							var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
						
						if(!data_2st_key_name)
							var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
						
						if(!data_3st_key_name)
							var data_3st_key_name = this.get_data_key_name(thisA.data, 2);

						var data_key_names = d3.keys(this.data[0]).filter(function(key) { return key !== data_1st_key_name; });
						
						this.adjust_data([data_2st_key_name, data_3st_key_name]);
						
						this._setup_xy(data_1st_key_name, data_2st_key_name, data_3st_key_name);
						
						thisA.config.window
						.append("rect").attr("class", "rect_main")
						.attr("width", thisA.config.width_)
						.attr("height", thisA.config.height_)
						.attr("fill", "#EEE")
						.on("dblclick.zoom", null)
						.call(
							d3.behavior
							.zoom()
							.scaleExtent([0.2, 5])
							.x(thisA.config.x0)
							.y(thisA.config.y0)
							
							.on("zoom", function(){
								thisA.config.window
								.selectAll(".main")
								.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
								thisA.draw_xaxis(data_2st_key_name, d3.event.translate, d3.event.scale);
								thisA.draw_yaxis(data_3st_key_name, d3.event.translate, d3.event.scale);
								thisA.x_axis_rotate();
							})
						);

						
						thisA.draw_xaxis(data_2st_key_name);
						thisA.draw_yaxis(data_3st_key_name);
						thisA.x_axis_rotate();
						thisA.draw_rect(data_1st_key_name, data_2st_key_name, data_3st_key_name)
						
						thisA.append_legend();

						thisA.mouse_event($(thisA.config.window[0]));
						thisA.add_png_button();
						
					});
				}
				,append_legend : function()
				{
					var thisA = this;
					var legend = thisA.config.window.selectAll(".legend")
					.data(thisA.config.color.domain())
					.enter().append("g")
					.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
					.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; })
					.style("fill", function(d){return thisA.config.color(d)});
					
					legend.append("rect")
					.attr("x", thisA.config.width_ - 18 +20)
					.attr("width", 18)
					.attr("height", 18)
					.style("fill", thisA.config.color);
					
					legend.append("text")
					.attr("x", thisA.config.width_ - 24 + 48)
					.attr("y", 9)
					.attr("dy", ".35em")
					.style("text-anchor", "start")
					.text(function(d) { return d; });
				}
				,draw_xaxis : function(text, translate, scale)
				{
					var thisA = this;
					if(!scale)	scale = 1;
					
					thisA.config.window.selectAll(".x_axis").remove();
					return thisA.config.window.append("g")
					.attr("class", "x_axis")
					.attr("transform", "translate(0," + thisA.config.height_ + ")")
					.call(thisA.config.xAxis)
					.append("text")
					.attr("x", thisA.config.width_)
					.attr("y", -10)
					.attr("dy", ".71em")
					.style("text-anchor", "end")
					.style("font-size", "10px")
					.text(text + " z: " + scale.toFixed(2));
				}
				,draw_yaxis : function(text, translate, scale)
				{
					var thisA = this;
					thisA.config.window.selectAll(".y_axis").remove();
					return thisA.config.window
					.append("g")
					.attr("class", "y_axis")
					.call(thisA.config.yAxis)
					.append("text")
					.attr("transform", "rotate(-90)")
					.attr("y", 6)
					.attr("dy", ".71em")
					.style("text-anchor", "end")
					.style("font-size", "10px")
					.text(text);
				}
				,draw_rect : function(data_1st_key_name, data_2st_key_name, data_3st_key_name)
				{
					var thisA = this;
					
					thisA.config.window
					.append("svg")
					.attr("width", thisA.config.width_)
					.attr("height", thisA.config.height_)
					.append("g")
					.attr("class", "main")
					.selectAll(".dot")
					.data(thisA.data)
					.enter().append("circle")
					.attr("class", 
						function(d, i)
						{
							return "dot item_class mouse_event item_order_"+ thisA.encode(d[ data_1st_key_name ] + i) + " item_class_" + thisA.encode(d[ data_1st_key_name ]);
						}
					)
					.attr("r", thisA.config.r)
					.attr("cx", function(d) { return thisA.config.x0(+d[ data_2st_key_name ]); })
					.attr("cy", function(d) { return thisA.config.y0(+d[ data_3st_key_name ]); })
					.attr("title", function(d) { return d[data_1st_key_name] +", "+ d[data_2st_key_name] +", "+ d[data_3st_key_name]; })
					.attr("fill", function(d) { return "#"+thisA.config.color(d[ data_1st_key_name ]); });
				}
				,_setup_xy : function(data_1st_key_name, data_2st_key_name, data_3st_key_name)
				{
					var thisA = this;
					
					if(!data_1st_key_name)
						var data_1st_key_name = this.get_data_key_name(thisA.data, 0);
					
					if(!data_2st_key_name)
						var data_2st_key_name = this.get_data_key_name(thisA.data, 1);
					
					if(!data_3st_key_name)
						var data_3st_key_name = this.get_data_key_name(thisA.data, 2);
					
					var min_max_x = d3.extent(thisA.data, function(d) { return d[data_2st_key_name]; });
					var min_max_y = d3.extent(thisA.data, function(d) { return d[data_3st_key_name]; });
					
					thisA.config.x0 = d3.scale.linear();
					
					thisA.config.x0
					.domain(min_max_x).nice()
					.range([0, thisA.config.width_]);
					
					thisA.config.y0
					.domain(min_max_y).nice()
					.range([thisA.config.height_, 0]);
					
					thisA.config.xAxis = d3.svg.axis().scale(thisA.config.x0).orient("bottom").tickSize(2);
					thisA.config.yAxis = d3.svg.axis().scale(thisA.config.y0).orient("left").tickSize(2);
					
					if(min_max_x[1] < 1)
						thisA.config.xAxis.ticks(10, "%");
					else
						thisA.config.xAxis.tickFormat(d3.format(".3s"));
					
					if(min_max_y[1] < 1)
						thisA.config.yAxis.ticks(10, "%");
					else
						thisA.config.yAxis.tickFormat(d3.format(".3s"));
						
					return [min_max_x, min_max_y];
				}
			}
			
		);
	
	}
);
