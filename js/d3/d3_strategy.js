/**
 *  @file d3_graph/js/d3/d3_helper.js
 *  @brief 常用功能
 *  @author C-Salt Corp.
 */
define(
	[
		"dojo/_base/declare"
		,"dojo/_base/lang"
		,"d3_graph/js/d3.v3.min.js"
		,"d3_graph/js/d3/d3_strategy_axis"
		,"d3_graph/js/d3/d3_strategy_content"
	],function(declare, lang, d3, D3_Strategy_axis, D3_Strategy_content){
		return declare( "D3_strategy", [D3_Strategy_axis, D3_Strategy_content],
			{
				constructor : function()
				{
				}
				,strategy : {}
				,calculate_basic_parameter: function()
				{
					var thisA = this;
					this.config.raw_keys = this.get_data_raw_keys(this.data);
					this.config.column_keys = this.get_data_column_keys(this.data);
					var raw_key_1st = this.config.raw_key_1st = this.config.raw_keys.shift();
					var raw_key_2st = this.config.raw_key_2st = this.config.raw_keys[0];
					var raw_key_3st = this.config.raw_key_3st = this.config.raw_keys[1];
					this.config.min_max_2st = d3.extent(this.data, function(d) { return +d[raw_key_2st]; });
					this.config.min_max_3st = d3.extent(this.data, function(d) { return +d[raw_key_3st]; });
					this.config.all_max = d3.max(this.data, function(d) {var m = 0;for(var key in thisA.config.raw_keys){m = Math.max(m, d[thisA.config.raw_keys[key]]);}return m;});
					this.config.color = d3.scale.ordinal().range(color2);
					
					/// @brief 主要目的為 string to int
					this.adjust_data([raw_key_2st]);
					
					///@brief copy data to origin_data
					if(this.origin_data.length == 0)
						this.data_copy(this.data, this.origin_data);
				}
				,make: function(strategy)
				{
					this.calculate_basic_parameter();
					
					if(strategy["color_domain"])
						this.color_domain(strategy["color_domain"]);
					if(strategy["data_handl"])
						this.data_handl(strategy["data_handl"]);
					if(strategy["make_axis"])
						this.make_axis(strategy["make_axis"]);
					if(strategy["draw_axis"])
						this.draw_axis(strategy["draw_axis"]);
					if(strategy["draw_content"])
						this.draw_content(strategy["draw_content"]);
					if(strategy["draw_legend"])
						this.draw_legend(strategy["draw_legend"]);

					this.mouse_event();
				}
				,color_domain: function(strategy_child)
				{
					var thisA = this;
					var type = strategy_child.type;
					switch(type)
					{	
						case "raw_keys":
							thisA.config.color.domain(thisA.config.raw_keys);
							break;
					}
				}
				,data_handl: function(strategy_child)
				{
					var thisA = this;
					var type = strategy_child.type;
					var limit_column = 99;
					
					// 目前沒有使用
					if(strategy_child.limit_column)
					{
						thisA.data_handl_limit_culomn_number(strategy_child.limit_column);
						thisA.calculate_basic_parameter();
					}
					
					if(strategy_child.transpose && !thisA.is_transposed)
					{
						thisA.data = thisA.transpose(thisA.data);
						console.log("strategy_child.transpose", thisA.data);
						thisA.calculate_basic_parameter();
						thisA.is_transposed = true;
					}
					
					switch(type)
					{
						case "StackedBarChart":
							thisA.data.forEach(function(d) {
							    var y0 = 0;
								d.data_array = thisA.config.raw_keys.map(function(name) {
									return {name: name, y0: y0, y1: y0 += +d[name]}; 
								});
								if(d.data_array[d.data_array.length - 1])
									d.total = + d.data_array[d.data_array.length - 1].y1;
							});
							break;
						case "GroupedBarChart":
							thisA.data.forEach(function(d) {
								d.data_array = thisA.config.raw_keys.map(function(name) {
									return {name: name, value: +d[name]}; 
								});
							});
							break;
						case "NormalizedBarChart":
							thisA.data.forEach(function(d) {
								var y0 = 0;
								d.data_array = thisA.config.raw_keys.map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
								d.data_array.forEach(function(d) { d.y0 /= y0; d.y1 /= y0;});
							});
							break;
					}
					
					
				}
				,data_handl_limit_culomn_number: function(num)
				{
					var thisA = this;
					
					for(var raw in thisA.data)
					{
						var i = 0;
						var other = 0;
						for(var col in thisA.data[raw])
						{
							if(i > num)
							{
								other += +thisA.data[raw][col];
								delete thisA.data[raw][col];
							}
							i++;
						}
						thisA.data[raw]["other"] = other;
					}
				}
				,draw_legend: function(strategy_child)
				{
					var thisA = this;
					var jWindow = thisA.config.window;
					var type = strategy_child.type;
					var limit_number = 99;
					
					if(strategy_child.limit)
						limit_number = strategy_child.limit;
					
					switch(type)
					{
						// pie chart 每個角裡面show words
						case "pie_column_center":
							//如果重劃，先刪除將要重劃的內容
							$(jWindow[0]).find("g.arc text").remove();
							for(var i=0;i<thisA.config.raw_keys.length; i++)
							{
								if(!thisA.config.pie[i]) break;
								var g = thisA.config.window
								.selectAll("g.arc")
								.append("text")
								.attr("transform", function(d) { return "translate(" + thisA.config.arc[i].centroid(d) + ")"; })
								.attr("dy", ".35em")
								.style("text-anchor", "middle")
								.text(function(d) {
									var svg_width = $(thisA.config.window[0]).parent().width();
									var svg_height = $(thisA.config.window[0]).parent().height();
									//console.log(svg_height, svg_width);
									svg_height -= (thisA.config.margin.top + thisA.config.margin.bottom + 60) * (svg_height/600);
									svg_width -= (thisA.config.margin.right + thisA.config.margin.left + 60) * (svg_width/1200);
									var new_radius = Math.min(svg_width, svg_height) / 2 ;
									var r_width = new_radius * (d.endAngle - d.startAngle);
									//console.log(new_radius, r_width, svg_height, svg_width);
									if(r_width < 26) return ""; // 如果圖太小，文字不顯示
									return d.data[thisA.config.raw_key_1st];
								});
							}
							// 下面也要做
							//break;
						case "column":
							var tmp_keys = thisA.config.column_keys.slice();
							tmp_keys.splice(limit_number, tmp_keys.length);
							var margin = Math.max(thisA.config.height_ / tmp_keys.length, 20);
							var legend = jWindow
							.append("g")
							.attr("class", "x_axis legends")
							.attr("transform", function(d, i) { return "translate("+ (thisA.config.width_ - 12) +",0)"; })
							.selectAll(".tick")
							.data(tmp_keys)
							.enter().append("g")
							.attr("class", function(d){return "tick item_class item_class_"+thisA.encode(d);})
							.attr("transform", function(d, i) { return "translate(0," + (i * margin + 60) + ")"; })
							.style("fill", function(d) { return thisA.config.color(d); });
							
							legend.append("rect")
							.attr("x", 20)
							.attr("width", 18)
							.attr("height", 18)
							.style("fill", thisA.config.color);
							
							legend.append("text")
							.attr("x", 48)
							.attr("y", 9)
							.attr("dy", ".35em")
							.style("text-anchor", "start")
							.text(function(d) { return d; });
							break;
						default:
							var tmp_keys = thisA.config.raw_keys.slice();
							tmp_keys.splice(limit_number, tmp_keys.length);
							var margin = Math.max(thisA.config.height_ / tmp_keys.length, 20);
							var legend = jWindow
							.append("g")
							.attr("class", "legends")
							.attr("transform", function(d, i) { return "translate("+ (thisA.config.width_ - 12) +",0)"; })
							.selectAll(".legend")
							.data(tmp_keys.reverse())
							.enter().append("g")
							.attr("class", function(d){return "legend item_class item_class_"+thisA.encode(d);})
							.attr("transform", function(d, i) { return "translate(0," + (i * margin + 60) + ")"; })
							.style("fill", function(d) { return thisA.config.color(d); });
							
							legend.append("rect")
							.attr("x", 20)
							.attr("width", 18)
							.attr("height", 18)
							.style("fill", thisA.config.color);
							
							legend.append("text")
							.attr("x", 48)
							.attr("y", 9)
							.attr("dy", ".35em")
							.style("text-anchor", "start")
							.text(function(d) { return d; });
							break;
					}
				}
				,draw_nodata: function()
				{
					var thisA = this;
					var jWindow = thisA.config.window;
					
					if(!this.check_is_nodata())
						return false;
					$(jWindow[0]).find("g").remove();
					
					var g = jWindow
					.append("g")
					.attr("class", "item_nodata")
					.attr("transform", "translate(-40,-30)");
					
					g.append("rect")
					.attr("x", thisA.config.margin.left/5)
					.attr("y", thisA.config.margin.top)
					.attr("width", thisA.config.width_*1)
					.attr("height", thisA.config.height_*1)
					.attr("stroke-width", thisA.config.width_*0.01)
					.attr("stroke", "#ccc")
					.style("fill", "#fff");
					
					g.append("text")
					.attr("x", (thisA.config.margin.left/2.5 + thisA.config.width_*1) /2)
					.attr("y", (thisA.config.margin.top + thisA.config.height_*0.9) /2.8)
					.attr("font-size", (thisA.config.width_*0.06) + "px")
					.attr("fill", "#ccc")
					.style("text-anchor", "middle")
					.attr("font-weight", 900)
					.text("NO DATA");
					
					if(!thisA.nodata_warning)
						return true;
					
					var y = (thisA.config.margin.top + thisA.config.height_*0.9)*1.9 /3;
					var split_string = thisA.nodata_warning.split(" ");
					var w = 0;
					for(var i=0; ; i++)
					{
						var string = "";
						// 字串自動換行，依造空白
						for( var nw = w; w != split_string.length; nw++)
						{
							if(string.length + split_string[nw].length > 24) break;
							string += split_string[nw] + " ";
							w++;
						}
						if(string == "") break;
						var ny = y + thisA.config.width_*0.08*i;
						g.append("text")
						.attr("x", (thisA.config.margin.left/2.5 + thisA.config.width_*1) /2)
						.attr("y", ny)
						.attr("font-size", (thisA.config.width_*0.04) + "px")
						.attr("fill", "#ccc")
						.style("text-anchor", "middle")
						.text(string);
					}						
					return true;
				}
			}
			
		);
	
	}
);
